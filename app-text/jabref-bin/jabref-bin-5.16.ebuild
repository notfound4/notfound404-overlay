# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit rpm xdg desktop

MY_P=jabref-${PV}
MY_PN=jabref

SRC_URI="https://builds.jabref.org/main/${MY_P}-1.x86_64.rpm"
DESCRIPTION="JabRef is an open-source, cross-platform citation and reference management tool."
HOMEPAGE="https://github.com/JabRef/jabref/"

LICENSE="MIT"
SLOT="0"
IUSE="-firefox -chromium"
KEYWORDS="~amd64"

RESTRICT="mirror"

# Need to test if the file can be unpacked with rpmoffset and cpio
# If it can't then set:

#DEPEND="app-arch/rpm"

# To force the use of rpmoffset and cpio instead of rpm2cpio from
# app-arch/rpm, then set the following:

#USE_RPMOFFSET_ONLY=1

S=${WORKDIR}/jabref-$(ver_cut 1-3)

src_unpack() {
    rpm_src_unpack ${A}
    mkdir ${S}
    mv ${WORKDIR}/opt ${S}
}

src_prepare() {
    eapply_user
}

src_install() {
    dodir /opt/${MY_PN}
    insinto /opt/
    doins -r ${S}/opt/jabref
    doicon ${S}/opt/jabref/lib/JabRef.png
    domenu ${S}/opt/jabref/lib/jabref-JabRef.desktop
    fperms 755 /opt/${MY_PN}/bin/JabRef /opt/${MY_PN}/lib/jabrefHost.py /opt/${MY_PN}/lib/runtime/lib/jexec
    fperms 755 /opt/${MY_PN}/lib/runtime/lib/jrt-fs.jar
    if use firefox; then
        exeinto /usr/lib/mozilla/native-messaging-hosts/
        doexec ${S}/opt/jabref/lib/native-messaging-host/firefox/org.jabref.jabref.json
    fi
    if use chromium; then
        exeinto /etc/chromium/native-messaging-hosts/
        doexec ${S}/opt/jabref/lib/native-messaging-host/chromium/org.jabref.jabref.json
    fi
    #fperms 755 /opt/${MY_PN}/runtime/jre/lib/management-agent.jar /opt/${MY_PN}/runtime/jre/lib/plugin.jar /opt/${MY_PN}/runtime/jre/lib/jfr.jar /opt/${MY_PN}/runtime/jre/lib/javaws.jar /opt/${MY_PN}/runtime/jre/lib/deploy.jar /opt/${MY_PN}/runtime/jre/lib/jce.jar /opt/${MY_PN}/runtime/jre/lib/resources.jar
}