# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Quarto is an academic, scientific, and technical publishing system built on Pandoc."
HOMEPAGE="https://github.com/quarto-dev/quarto-cli/"
SRC_URI="
amd64? ( https://github.com/quarto-dev/${PN}/releases/download/v${PV}/quarto-${PV}-linux-amd64.deb )"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

RESTRICT="mirror"

src_unpack() {
	if [[ -n ${A} ]]; then
		unpack ${A}
		mkdir ${S}
		tar xzvf data.tar.gz
		mv opt/ "${S}/opt"
	fi
}

src_install() {
	insinto /opt
	doins -r opt/quarto
	chmod ugo+x "${ED}/opt/quarto/bin/quarto"
	chmod ugo+x "${ED}/opt/quarto/bin/tools/x86_64/deno"
	chmod ugo+x "${ED}/opt/quarto/bin/tools/x86_64/esbuild"
	chmod ugo+x "${ED}/opt/quarto/bin/tools/x86_64/pandoc"
	chmod ugo+x "${ED}/opt/quarto/bin/tools/x86_64/typst"
	chmod ugo+x "${ED}/opt/quarto/bin/tools/x86_64/dart-sass/sass"
	chmod ugo+x "${ED}/opt/quarto/bin/tools/x86_64/dart-sass/src/dart"
	dosym "${EPREFIX}/opt/quarto/bin/quarto" /usr/bin/quarto
	dosym "${EPREFIX}/opt/quarto/share/man/quarto.man" /usr/share/man/man1/quarto.1
	dosym "${EPREFIX}/opt/quarto/bin/tools/x86_64/pandoc" /opt/quarto/bin/tools/pandoc
}
