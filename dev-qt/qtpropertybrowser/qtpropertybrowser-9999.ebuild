# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake git-r3

DESCRIPTION="A property browser framework enabling the user to edit a set of properties"
HOMEPAGE="https://github.com/abhijitkundu/QtPropertyBrowser"

EGIT_REPO_URI="https://github.com/abhijitkundu/QtPropertyBrowser.git"

LICENSE=""
KEYWORDS="~amd64 ~arm ~x86 ~amd64-linux ~x86-linux"
SLOT="0"

RDEPEND="
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtxml:5
	dev-qt/qtwidgets:5
"

DEPEND="${RDEPEND}"

RESTRICT=test

PATCHES=(
	"${FILESDIR}/path_correction.patch"
)

src_configure() {
	# general configuration
	local mycmakeargs=(
		# General
		-DBUILD_EXAMPLES=OFF
		-DCMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT=OFF
		-DINSTALL_LIB_DIR="lib64"
		-DINSTALL_CMAKE_DIR="lib64/cmake/QtPropertyBrowser"
	)

	cmake_src_configure
}