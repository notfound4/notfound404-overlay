# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit desktop xdg-utils

MY_PN=${PN/-/_}

DESCRIPTION="A new Git Client, from the creators of Sublime Text"
HOMEPAGE="https://www.sublimemerge.com"
SRC_URI="
	amd64? ( https://download.sublimetext.com/${MY_PN}_build_${PV}_x64.tar.xz )
"

LICENSE="Sublime"
SLOT="0"
KEYWORDS="~amd64"
IUSE="dbus"
RESTRICT="bindist mirror strip"

RDEPEND="
	dev-vcs/git
	dev-libs/glib:2
	x11-libs/gtk+:3
	x11-libs/libX11
	dbus? ( sys-apps/dbus )
"

QA_PREBUILD="*"
S="${WORKDIR}/${MY_PN}"

#src_prepare(){
#	sed -i -e "/Actions/d" "${MY_PN}.desktop"
#	default
#}

src_install(){
	insinto /opt/${MY_PN}
	doins -r Packages Icon
	doins changelog.txt

	exeinto /opt/${MY_PN}
	doexe crash_reporter git-credential-sublime ssh-askpass-sublime sublime_merge
	dosym ../../opt/${MY_PN}/sublime_merge /usr/bin/sublime_merge

	local size
	for size in 16 32 48 128 256; do
		dosym ../../../../../../opt/${MY_PN}/Icon/${size}x${size}/sublime-merge.png \
			/usr/share/icons/hicolor/${size}x${size}/apps/sublime_merge.png
	done

	make_desktop_entry "sublime_merge %F" "Sublime Merge" sublime_merge \
		"Development" "StartupNotify=true"
}

pkg_postrm() {
	xdg_icon_cache_update
}

pkg_postinst() {
	xdg_icon_cache_update
}