# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

# shellcheck disable=SC2034
EAPI=7

DESCRIPTION="Banish the mouse cursor when typing, show it again when the mouse moves."
HOMEPAGE="https://github.com/jcs/xbanish"

SRC_URI="https://github.com/jcs/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~x86"

LICENSE="GPL-3"
SLOT="0"

RDEPEND="
	x11-libs/libX11
	x11-libs/libXfixes
	x11-libs/libXi
"

DEPEND="
	${RDEPEND}
"

src_install() {
	if [[ -f Makefile ]] || [[ -f GNUmakefile ]] || [[ -f makefile ]] ; then
		emake PREFIX="/usr" MANDIR="/usr/share/man/man1" DESTDIR="${D}" install
	fi
	einstalldocs
}
