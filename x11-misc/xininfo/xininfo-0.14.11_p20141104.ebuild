# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools git-r3

EGIT_REPO_URI="https://github.com/DaveDavenport/xininfo.git"
EGIT_COMMIT="0bd94ad8f0a94fc5b3869cc9029a7d1d0b796981"

DESCRIPTION="A tool to query the layout and size of each configured monitor."
HOMEPAGE="https://github.com/DaveDavenport/xininfo"

LICENSE="MIT/X11"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	x11-libs/libxcb
	x11-libs/xcb-util
	x11-libs/xcb-util-wm
	x11-libs/xcb-util-xrm
"
DEPEND="${RDEPEND}"

src_prepare() {
	# Rerun autotools
	einfo "Regenerating autotools files..."
	eapply_user
   	eautoreconf
}

src_compile() {
	econf
	emake
}