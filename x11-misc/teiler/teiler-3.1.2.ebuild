# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

EGIT_REPO_URI="https://github.com/carnager/teiler.git"

DESCRIPTION="A little screenshot tool written in bash"
HOMEPAGE="https://github.com/carnager/teiler"
SRC_URI="https://github.com/carnager/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-v3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="imgur filebin openssh"

RDEPEND="
	x11-misc/xininfo
	x11-misc/rofi
	media-video/ffmpeg
	x11-misc/xclip
	media-gfx/maim
	x11-misc/slop
	x11-misc/copyq
	imgur? ( net-misc/imgurbash2 )
	filebin? ( net-misc/filebin )
	openssh? ( net-misc/openssh )
"
DEPEND="${RDEPEND}"

src_compile() {
	einfo "Compiling nothing"
}

src_install() {
	dodoc config.example
	einstalldocs

	dobin teiler
	dobin teiler_helper

	insinto /etc/teiler
	newins config.example teiler.conf
	insinto /etc/teiler/uploader
	doins uploader/s3
	doins uploader/scp
	insinto /etc/teiler/profiles
	doins profiles/mp4-pulse
	doins profiles/mp4-noaudio
	doins profiles/webm-noaudio
}