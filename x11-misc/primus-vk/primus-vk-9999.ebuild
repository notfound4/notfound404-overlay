# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit flag-o-matic git-r3

EGIT_REPO_URI="https://github.com/felixdoerre/primus_vk.git"

DESCRIPTION="Primus-Vk Nvidia Vulkan offloading for Bumblebee"
HOMEPAGE="https://github.com/felixdoerre/primus_vk"

KEYWORDS="~amd64 ~x86"

LICENSE="BSD"
SLOT="0"

RDEPEND="
	media-libs/vulkan-loader[layers]
	x11-misc/bumblebee
	x11-misc/primus
	x11-drivers/nvidia-drivers
"

DEPEND="
	${RDEPEND}
"

src_compile() {
	append-flags '-DNV_DRIVER_PATH=\"/usr/lib/opengl/nvidia/lib/libGLX_nvidia.so.0\"'
	emake
}

src_install() {
	dolib.so libnv_vulkan_wrapper.so
	dolib.so libprimus_vk.so

	insinto /etc/vulkan/icd.d
	doins nv_vulkan_wrapper.json
	insinto /etc/vulkan/implicit_layer.d
	doins primus_vk.json

	newbin pvkrun.in.sh pvkrun

	einstalldocs
}