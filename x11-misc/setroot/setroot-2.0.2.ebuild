# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Simple X background setter inspired by imlibsetroot and feh."
HOMEPAGE="https://github.com/ttzhou/setroot"

SRC_URI="https://github.com/ttzhou/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~x86"

LICENSE="GPL-3"
SLOT="0"

IUSE="xinerama"

RDEPEND="
	media-libs/imlib2
	xinerama? ( x11-libs/libXinerama )
"

DEPEND="
	${RDEPEND}
"

src_compile() {
	emake xinerama=1
}

src_install() {
	if [[ -f Makefile ]] || [[ -f GNUmakefile ]] || [[ -f makefile ]] ; then
		emake PREFIX="/usr" DESTDIR="${D}" install
	fi
	einstalldocs
}