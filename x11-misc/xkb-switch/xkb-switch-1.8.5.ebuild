# Copyright 2020-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit cmake

DESCRIPTION="Program that allows to query and change the XKB layout state."
HOMEPAGE="https://github.com/grwlf/xkb-switch"
SRC_URI="https://github.com/grwlf/${PN}/archive/refs/tags/${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

PATCHES=(
	"${FILESDIR}/cmake_install.patch"
)

# src_prepare() {
# 	sed -e '25s/.*/if(FALSE)/' -e 's/^add_sanitizers(.*//' \
# 		-i CMakeLists.txt
# 	cmake_src_prepare
# }

# src_configure() {
# 	local mycmakeargs=(
# 		-DBUILD_TESTS=OFF
# 		-DBUILD_TOOLS=OFF
# 		-DBUILD_SHARED_LIBS=ON
# 	)
# 	cmake_src_configure
# }
