# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..11} )

inherit toolchain-funcs cmake python-single-r1

MYPN=ITK
MYP=${MYPN}-${PV}

DESCRIPTION="NLM Insight Segmentation and Registration Toolkit"
HOMEPAGE="http://www.itk.org"
SRC_URI="https://github.com/InsightSoftwareConsortium/ITK/archive/v${PV}.tar.gz -> ${P}.tar.gz
	doc? ( mirror://sourceforge/${PN}/InsightDoxygenDocHtml-4.13.0.tar.gz )"
RESTRICT="primaryuri"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"
IUSE="debug doc examples fftw itkv4compat opencl python review sse2 test vtkglue -bridgenumpy"

RDEPEND="
	dev-libs/double-conversion:0=
	media-libs/libpng:0=
	media-libs/tiff:0=
	sci-libs/dcmtk
	dev-libs/expat
	sci-libs/gdcm
	sci-libs/nifticlib
	sci-libs/hdf5:0=[cxx]
	sys-libs/zlib:0=
	virtual/jpeg:0=
	fftw? ( sci-libs/fftw:3.0= )
	vtkglue? ( sci-libs/vtk:0=[python?] )
	opencl? ( dev-libs/ocl-icd )
	dev-cpp/eigen
"
DEPEND="${RDEPEND}
	python? ( ${PYTHON_DEPS}
			  >=dev-lang/swig-3.0
			  >=dev-cpp/CastXML-0.0.0_pre1 )
	doc? ( app-doc/doxygen )
"

REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"

S="${WORKDIR}/${MYP}"

#PATCHES=(
#	"${FILESDIR}/remove_gcc_version_test.patch"
#)

pkg_pretend() {
	if [[ -z ${ITK_COMPUTER_MEMORY_SIZE} ]]; then
		elog "To tune ITK to make the best use of working memory you can set"
		elog "    ITK_COMPUTER_MEMORY_SIZE=XX"
		elog "in make.conf, default is 1 (unit is GB)"
	fi
	if use python && [[ -z ${ITK_WRAP_DIMS} ]]; then
		elog "For Python language bindings, you can define the dimensions"
		elog "you want to create bindings for by setting"
		elog "    ITK_WRAP_DIMS=X;Y;Z..."
		elog "in make.conf, default is 2;3 for 2D and 3D data"
	fi
}

src_configure() {



	local mycmakeargs=(
		-DBUILD_SHARED_LIBS=ON
		-DITK_USE_GPU="$(usex opencl)"
		-DITK_USE_SYSTEM_DCMTK=ON
		-DITK_USE_SYSTEM_DOUBLECONVERSION=ON
		-DITK_USE_SYSTEM_EXPAT=ON
		-DITK_USE_SYSTEM_EIGEN=ON
		-DITK_USE_SYSTEM_GDCM=ON
		-DITK_USE_SYSTEM_GOOGLETEST=OFF
		-DITK_USE_SYSTEM_HDF5=ON
		-DITK_USE_SYSTEM_JPEG=ON
		-DITK_USE_SYSTEM_KWIML=OFF
		-DITK_USE_SYSTEM_LIBRARIES=ON
		-DITK_USE_SYSTEM_MINC=OFF
		-DITK_USE_SYSTEM_PNG=ON
		-DITK_USE_SYSTEM_TIFF=ON
		-DITK_USE_SYSTEM_VXL=OFF
		-DITK_USE_SYSTEM_ZLIB=ON
		-DITK_BUILD_DEFAULT_MODULES=ON
		-DITK_COMPUTER_MEMORY_SIZE="${ITK_COMPUTER_MEMORY_SIZE:-1}"
		-DModule_ITKDCMTK=ON
		-DModule_ITKIODCMTK=OFF
		-DModule_ITKBridgeNumPy="$(usex bridgenumpy)"
		-DModule_ITKFEM=ON
		-DModule_ITKFEMRegistration=ON
		-DBUILD_TESTING="$(usex test)"
		-DBUILD_EXAMPLES="$(usex examples)"
		-DModule_ITKReview="$(usex review)"
		-DITKV4_COMPATIBILITY="$(usex itkv4compat)"
		-DITK_INSTALL_LIBRARY_DIR:STRING=$(get_libdir)
		-DITK_INSTALL_DOC_DIR:STRING="share/doc/${P}"
		-DVNL_CONFIG_ENABLE_SSE2_ROUNDING="$(usex sse2)"
	)

	if use fftw; then
		mycmakeargs+=(
			-DUSE_FFTWD=ON
			-DUSE_FFTWF=ON
			-DUSE_SYSTEM_FFTW=ON
		)
	fi
	if use vtkglue; then
		mycmakeargs+=(
			-DModule_ITKVtkGlue=ON
		)
	fi
	if use python; then
		mycmakeargs+=(
			-DITK_WRAP_PYTHON=ON
			-DITK_WRAP_DIMS="${ITK_WRAP_DIMS:-2;3}"
		)
		if use bridgenumpy; then
			mycmakeargs+=(
				-DITK_FORBID_DOWNLOADS=OFF
		        	-DModule_BridgeNumPy:BOOL=ON
			)
		else
			mycmakeargs+=(
				-DITK_FORBID_DOWNLOADS=ON
			        -DModule_BridgeNumPy:BOOL=OFF
			)
		fi
	else
		mycmakeargs+=(
			-DITK_WRAP_PYTHON=OFF
			-DITK_FORBID_DOWNLOADS=ON
		)
	fi
	cmake_src_configure
}

src_install() {
	cmake_src_install

	if use examples; then
		insinto /usr/share/doc/${PF}/examples
		docompress -x /usr/share/doc/${PF}/examples
		doins -r "${S}"/Examples/*
	fi

	echo "ITK_DATA_ROOT=${EROOT%/}/usr/share/${PN}/data" > ${T}/40${PN}
	local ldpath="${EROOT%/}/usr/$(get_libdir)/InsightToolkit"
	if use python; then
		echo "PYTHONPATH=${EROOT%/}/usr/$(get_libdir)/InsightToolkit/WrapITK/Python" >> "${T}"/40${PN}
		ldpath="${ldpath}:${EROOT%/}/usr/$(get_libdir)/InsightToolkit/WrapITK/lib"
	fi
	echo "LDPATH=${ldpath}" >> "${T}"/40${PN}
	doenvd "${T}"/40${PN}

	if use doc; then
		insinto /usr/share/doc/${PF}/api-docs
		cd "${WORKDIR}"/html
		rm  *.md5 || die "Failed to remove superfluous hashes"
		einfo "Installing API docs. This may take some time."
		insinto /usr/share/doc/${PF}/api-docs
		doins -r *
	fi
}