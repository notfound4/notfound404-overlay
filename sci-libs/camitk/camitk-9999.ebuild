# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{9..11} )

inherit python-single-r1 cmake git-r3

DESCRIPTION="Computer Assisted Medical Intervention Tool Kit"
HOMEPAGE="http://camitk.imag.fr"

EGIT_REPO_URI="https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK.git"
EGIT_BRANCH="develop"

CMAKE_MAKEFILE_GENERATOR="emake"

LICENSE="LGPL-3"
KEYWORDS="~amd64 ~arm ~x86 ~amd64-linux ~x86-linux"
SLOT="0"
IUSE="all-modules app-asm app-cepgen app-imp app-wizard cep-imaging cep-modeling doc python sofa"

REQUIRED_USE="
	all-modules? ( app-asm app-cepgen app-imp app-wizard cep-imaging cep-modeling )
	python? ( ${PYTHON_REQUIRED_USE} )
"

RDEPEND="
	dev-qt/qtxmlpatterns:5
	dev-qt/qthelp:5
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtxml:5
	dev-qt/qtwidgets:5
	dev-qt/qtopengl:5
	dev-qt/designer:5
	dev-qt/qtx11extras:5
	>=sci-libs/vtk-6.0.0[all-modules]
	dev-libs/xerces-c
	dev-cpp/xsd
	$(python_gen_cond_dep "
	python? (
		${PYTHON_DEPS}
		dev-python/shiboken[\${PYTHON_USEDEP}]
		dev-python/sip[\${PYTHON_USEDEP}]
		dev-python/PyQt5[\${PYTHON_USEDEP}]
		dev-python/pyside[\${PYTHON_USEDEP}]
	)
	")
	cep-imaging? (
		sci-libs/gdcm
		sci-libs/itk
		sci-libs/gdcm[vtk]
	)
	sofa? (
		sci-libs/sofa
	)
"

DEPEND="${RDEPEND}
	doc? ( app-doc/doxygen )
"

RESTRICT=test

#PATCHES=(
#	"${FILESDIR}/itk5.patch"
#)

pkg_setup() {
	use python && python-single-r1_pkg_setup
}

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	# general configuration
	local mycmakeargs=(
		# General
		-DAPIDOC_GENERATE_FOR_LML=$(usex doc)
		-DAPIDOC_GENERATE_FOR_PML=$(usex doc)
		-DAPIDOC_SDK=$(usex doc)
		-DBUILD_TESTING=ON
		-DPML_TOOLS=OFF
		# Actions
		-DACTION_APPLICATION=ON
		-DACTION_BASICMESH=ON
		-DACTION_BASICTOPOLOGY=ON
		-DACTION_CROPVOLUME=ON
		-DACTION_EDITFRAMES=ON
		-DACTION_IMAGEACQUISITION=ON
		-DACTION_IMAGELUT=ON
		-DACTION_IMAGERESAMPLING=ON
		-DACTION_ITKFILTERS=$(usex cep-imaging)
		-DACTION_ITKSEGMENTATION=$(usex cep-imaging)
		-DACTION_MESHPROCESSING=ON
		-DACTION_MML=$(usex cep-modeling)
		-DACTION_MULTIPICKING=ON
		-DACTION_PIXELCOLORCHANGER=ON
		-DACTION_PML=$(usex cep-modeling)
		-DACTION_RECONSTRUCTION=ON
		-DACTION_REORIENTIMAGE=ON
		-DACTION_SHOWIN3D=ON
		-DACTION_VOLUMERENDERING=ON
		# Applications
		-DAPPLICATION_ACTIONSTATEMACHINE=$(usex app-asm)
		-DAPPLICATION_CEPGENERATOR=$(usex app-cepgen)
		-DAPPLICATION_CONFIG=ON
		-DAPPLICATION_IMP=$(usex app-imp)
		-DAPPLICATION_TESTACTIONS=ON
		-DAPPLICATION_TESTCOMPONENTS=ON
		-DAPPLICATION_WIZARD=$(usex app-wizard)
		# Camitk
		-DCAMITK_BINDING_PYTHON=$(usex python)
		# CEPs
		-DCEP_IMAGING=$(usex cep-imaging)
		-DCEP_MODELING=$(usex cep-modeling)
		# Components
		-DCOMPONENT_DICOM=$(usex cep-imaging)
		-DCOMPONENT_ITKIMAGE=$(usex cep-imaging)
		-DCOMPONENT_MMLCOMPONENT=$(usex cep-modeling)
		-DCOMPONENT_MSH=ON
		-DCOMPONENT_OBJ=ON
		-DCOMPONENT_OFF=ON
		-DCOMPONENT_PMLCOMPONENT=$(usex cep-modeling)
		-DCOMPONENT_STL=ON
		-DCOMPONENT_VRML=ON
		-DCOMPONENT_VTKIMAGE=ON
		-DCOMPONENT_VTKMESH=ON
		# MML
		-DMML_GENERATE_BENCHMARK=$(usex cep-modeling)
		-DMML_GENERATE_BENCHMARKGUI=$(usex cep-modeling)
		-DMML_GENERATE_GUI=$(usex cep-modeling)
		-DMML_GENERATE_PARAMEXPLORER=$(usex cep-modeling)
		-DMML_GENERATE_PML2MMLOUT=$(usex cep-modeling)
	)

	cmake_src_configure
}

src_test() {
	cmake_src_test
}

src_install() {
	cmake_src_install

	local ldpath="${EROOT%/}/usr/$(get_libdir)/camitk-5.0:${EROOT%/}/usr/$(get_libdir)/camitk-5.0/components:${EROOT%/}/usr/$(get_libdir)/camitk-5.0/actions:${EROOT%/}/usr/$(get_libdir)/camitk-5.0/viewers"
	echo "LDPATH=${ldpath}" >> "${T}"/40${PN}
	doenvd "${T}"/40${PN}

	#install big docs
	if use doc; then
		rm -f "${WORKDIR}"/html/*.md5 || die "Failed to remove superfluous hashes"
		einfo "Installing API docs. This may take some time."
		dodoc -r "${WORKDIR}"/html
	fi
}