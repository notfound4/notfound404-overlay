# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..12} )

inherit distutils-r1

DESCRIPTION="An improved console for the R program with multiline editing and rich syntax highlight and more."
HOMEPAGE="https://github.com/randy3k/radian"
SRC_URI="https://github.com/randy3k/${PN}/archive/v${PVR}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	dev-lang/R
	>=dev-python/prompt-toolkit-3.0.41[${PYTHON_USEDEP}]
	!>=dev-python/prompt-toolkit-3.1.0
	>=dev-python/rchitect-0.4.6[${PYTHON_USEDEP}]
	!>=dev-python/rchitect-0.5.0
	>=dev-python/six-1.9.0[${PYTHON_USEDEP}]
	>=dev-python/pygments-2.5.0[${PYTHON_USEDEP}]
"

DEPEND="${RDEPEND}
	dev-python/pytest[${PYTHON_USEDEP}]
"

PATCHES=(
	"${FILESDIR}/radian-0.6.1-setup.patch"
)
