# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="System tray application for monitoring charge level of connecteds devices."
HOMEPAGE="https://gitlab.com/n3974/batteries-status"

SRC_URI="https://gitlab.com/n3974/${PN}/-/archive/${PV}/${PN}-${PV}.tar.bz2"
KEYWORDS="~amd64 ~arm64 ~x86"

LICENSE="GPL-3"
SLOT="0"
REQUIRED_USE=""

DEPEND="
	dev-qt/qtbase:6[widgets]
	x11-libs/libnotify
"
RDEPEND="
	${DEPEND}
"
BDEPEND="
	dev-build/cmake
	virtual/pkgconfig
"
src_configure() {
    local mycmakeargs=(
        -DCMAKE_INSTALL_SYSCONFDIR=/etc
    )
    cmake_src_configure
}
