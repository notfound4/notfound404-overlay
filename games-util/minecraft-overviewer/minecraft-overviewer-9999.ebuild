# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

PYTHON_COMPAT=( python3_{7..10} )

inherit distutils-r1 git-r3

DESCRIPTION="A command line tool for rendering high-resolution maps of Minecraft worlds."
HOMEPAGE="https://www.github.com/overviewer/Minecraft-Overviewer"
EGIT_REPO_URI="https://github.com/overviewer/Minecraft-Overviewer.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	${PYTHON_DEPENDS}
	dev-python/pillow[${PYTHON_USEDEP}]
	dev-python/numpy[${PYTHON_USEDEP}]
"
RDEPEND="${DEPEND}"

python_install_all() {
	distutils-r1_python_install_all
}
