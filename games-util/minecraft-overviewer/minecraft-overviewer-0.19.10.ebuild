# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

MY_PN="overviewer"
MY_P="${MY_PN}-${PV}"

MY_P_WORK="Minecraft-Overviewer-${PV}"

PYTHON_COMPAT=( python3_{9..12} )

inherit distutils-r1

DESCRIPTION="A command line tool for rendering high-resolution maps of Minecraft worlds."
HOMEPAGE="https://www.github.com/overviewer/Minecraft-Overviewer"
SRC_URI="https://overviewer.org/builds/src/284/${MY_P}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"

DEPEND="
	${PYTHON_DEPENDS}
	dev-python/pillow[${PYTHON_USEDEP}]
	dev-python/numpy[${PYTHON_USEDEP}]
"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${MY_P_WORK}"

RESTRICT="test"

src_prepare() {
	sed -i -e "s:share/doc/${PN}:share/doc/${P}:" setup.py || die "Sed failed!"
	eapply_user
}

python_install_all() {
	distutils-r1_python_install_all
}
