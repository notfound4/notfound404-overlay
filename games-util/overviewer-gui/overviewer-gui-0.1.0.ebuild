# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..11} )

inherit distutils-r1

DESCRIPTION="A graphical user interface for minecraft overviewer"
HOMEPAGE="https://gitlab.com/notfound4/overviewer-gui"
SRC_URI="https://gitlab.com/notfound4/overviewer-gui/-/archive/${PV}/${PF}.tar.gz"

LICENSE="CC0"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	dev-python/automat[${PYTHON_USEDEP}]
	dev-python/setuptools[${PYTHON_USEDEP}]
"

DEPEND="${RDEPEND}"

python_install_all() {
	distutils-r1_python_install_all

	insinto /usr/share/applications
	doins doc/overviewer-gui.desktop
	
	insinto /usr/share/pixmaps
	doins doc/overviewer-gui.xpm
	
	insinto /usr/share/icons/hicolor/48x48/apps
	doins doc/overviewer-gui.png
}