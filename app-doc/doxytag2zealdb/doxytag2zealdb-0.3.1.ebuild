# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

PYTHON_COMPAT=( python3_{9..13} )

inherit distutils-r1

DESCRIPTION="doxytag2zealdb creates a SQLite3 database from a Doxygen tag file to enable searchable Doxygen docsets with categorized entries in tools like helm-dash, Zeal, and Dash."
HOMEPAGE="https://gitlab.com/vedvyas/doxytag2zealdb"
SRC_URI="https://files.pythonhosted.org/packages/31/2a/d848482eab1eae103bf433ae2489facdc34a4b4756a8033128eefa005b7f/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	>=dev-python/beautifulsoup4-4.4.1[${PYTHON_USEDEP}]
	>=dev-python/lxml-3.6.0[${PYTHON_USEDEP}]
	>=dev-python/docopt-0.6.2[${PYTHON_USEDEP}]
	dev-python/future[${PYTHON_USEDEP}]
"

DEPEND="${RDEPEND}"
