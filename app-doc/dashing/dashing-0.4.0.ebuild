# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit go-module
EGO_SUM=(
	"github.com/BurntSushi/toml v0.3.1/go.mod"
	"github.com/andybalholm/cascadia v1.1.1-0.20191115165331-903109d295d5/go.mod"
	"github.com/andybalholm/cascadia v1.1.1-0.20191115165331-903109d295d5"
	"github.com/mattn/go-sqlite3 v2.0.1+incompatible/go.mod"
	"github.com/mattn/go-sqlite3 v2.0.1+incompatible"
	"github.com/urfave/cli/v2 v2.0.0/go.mod"
	"github.com/urfave/cli/v2 v2.0.0"
	"golang.org/x/net v0.0.0-20180218175443-cbe0f9307d01/go.mod"
	"golang.org/x/net v0.0.0-20191207000613-e7e4b65ae663/go.mod"
	"golang.org/x/net v0.0.0-20191207000613-e7e4b65ae663"
	"github.com/cpuguy83/go-md2man/v2 v2.0.0-20190314233015-f79a8a8ca69d/go.mod"
	"github.com/cpuguy83/go-md2man/v2 v2.0.0-20190314233015-f79a8a8ca69d"
	"github.com/russross/blackfriday/v2 v2.0.1/go.mod"
	"github.com/russross/blackfriday/v2 v2.0.1"
	"github.com/shurcooL/sanitized_anchor_name v1.0.0/go.mod"
	"github.com/shurcooL/sanitized_anchor_name v1.0.0"
	"gopkg.in/yaml.v2 v2.2.2/go.mod"
	"golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2/go.mod"
	"golang.org/x/sys v0.0.0-20190215142949-d0b11bdaac8a/go.mod"
	"golang.org/x/text v0.3.0/go.mod"
	"github.com/pmezard/go-difflib v1.0.0"
	"github.com/pmezard/go-difflib v1.0.0/go.mod"
	"gopkg.in/check.v1 v0.0.0-20161208181325-20d25e280405/go.mod"
)
EGO_PN="github.com/technosophos/dashing"

go-module_set_globals

DESCRIPTION="Generate Dash documentation from HTML."
HOMEPAGE="https://github.com/technosophos/dashing"
SRC_URI="https://${EGO_PN}/archive/${PV}.tar.gz -> ${P}.tar.gz
${EGO_SUM_SRC_URI}"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
#src_prepare() {
#	mkdir -p vendor/github.com/urfave/v2
#	mv vendor/github.com/urfave/cli/* vendor/github.com/urfave/v2
#	mv vendor/github.com/urfave/v2 vendor/github.com/urfave/cli/v2
#
#	mkdir -p vendor/github.com/cpuguy83/v2
#	mv vendor/github.com/cpuguy83/go-md2man/* vendor/github.com/cpuguy83/v2
#	mv vendor/github.com/cpuguy83/v2 vendor/github.com/cpuguy83/go-md2man/v2
#
#	mkdir -p vendor/github.com/russross/v2
#	mv vendor/github.com/russross/blackfriday/* vendor/github.com/russross/v2
#	mv vendor/github.com/russross/v2 vendor/github.com/russross/blackfriday/v2
#
#	mkdir -p vendor/golang.org/x
#	mv vendor/github.com/golang/net vendor/golang.org/x/net
#
#	eapply_user
#}

src_compile() {
	go build -o dashing -ldflags "-X main.version=${PV}" dashing.go || die
}

src_install() {
	dodoc README.md
	dobin dashing
}