# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: node.eclass
# @MAINTAINER:
# Jannis Mast <jannis@ctrl-c.xyz>
# @SUPPORTED_EAPIS: 7
# @BLURB: eclass for node.js packages
# @DESCRIPTION:
# This eclass takes care of installing node.js packages and their dependencies

case ${EAPI:-0} in
	0|1|2|3|4|5|6)
		die "EAPI=${EAPI} is not supported by node.eclass (too old)"
		;;
	7|8)
		;;
	*)
		die "EAPI=${EAPI} is not supported by node.eclass"
		;;
esac

EXPORT_FUNCTIONS src_unpack src_install

# @ECLASS-VARIABLE: NODE_MODULE_NAME
# @DESCRIPTION:
# The name of the node module that belongs to the package.
# Defaults to ${PN} if not set
if [ -z ${NODE_MODULE_NAME} ]; then
	NODE_MODULE_NAME="${PN}"
fi

# @ECLASS-VARIABLE: NODE_RELEASE_FOLDER
# @DESCRIPTION:
# The name of the folder to install.
# Defaults to linux-unpacked if not set
if [ -z ${NODE_RELEASE_FOLDER} ]; then
	NODE_RELEASE_FOLDER="linux-unpacked"
fi

# @ECLASS_VARIABLE: NODE_DEPEND
# @DESCRIPTION:
# A list of all node.js modules that this package depends on
# Entries should have the format "name:version"
# @ECLASS_VERIABLE: NODE_BIN
# @DESCRIPTION:
# List of scripts that should be linked into /usr/bin
# Entries should have the format "name:path"
# where "name" is the destination filename in /usr/bin
# and "path" is the relative path of the script in the module's directory
# @ECLASS_VARIABLE: NODEJS_MIN_VERSION
# @DESCRIPTION:
# Minimum version of net-libs/noejs for this package (optional)

if [ -z ${SRC_URI+x} ]; then
	SRC_URI="http://registry.npmjs.org/${NODE_MODULE_NAME}/-/${NODE_MODULE_NAME}-${PV}.tgz"
fi
for pkg in ${NODE_DEPEND}; do
	if [[ $pkg == @* ]]; then
		pkg_cat="${pkg%/*}"
		tmp="${pkg%:*}"
		pkg_name="${tmp#*/}"
		pkg_version="${pkg#*:}"
		SRC_URI="${SRC_URI}
		http://registry.npmjs.org/${pkg_cat}/${pkg_name}/-/${pkg_name}-${pkg_version}.tgz"
	else
		pkg_name="${pkg%:*}"
		if [[ $pkg_name == github ]]; then
			tmp="${pkg#*:}"
			git_user="${tmp%/*}"
			git_name="${tmp#*/}"
			if [[ $git_name == *#* ]]; then
				git_commit="${git_name#*#}"
				git_name="${git_name%#*}"
				SRC_URI="${SRC_URI}
				http://github.com/${git_user}/${git_name}/archive/${git_commit:0:7}.tar.gz -> ${git_name}-${git_commit}.tar.gz"
			else
				SRC_URI="${SRC_URI}
				http://github.com/${git_user}/${git_name}/archive/master.tar.gz -> ${git_name}-master.tar.gz"
			fi
		else
			pkg_version="${pkg#*:}"
			SRC_URI="${SRC_URI}
			http://registry.npmjs.org/${pkg_name}/-/${pkg_name}-${pkg_version}.tgz"
		fi
	fi
done
S="${WORKDIR}/${NODE_MODULE_NAME}"

DEPEND=""
if [ -z ${NODEJS_MIN_VERSION} ]; then
	RDEPEND="net-libs/nodejs"
else
	RDEPEND=">=net-libs/nodejs-${NODEJS_MIN_VERSION}"
fi

node_src_unpack() {
	unpack "${NODE_MODULE_NAME}-${PV}.tgz"
	if [[ -d "package" ]]; then
		mv "package" "${NODE_MODULE_NAME}" || die
	elif [[ -d "${NODE_MODULE_NAME}-${PV}" ]]; then
		mv "${NODE_MODULE_NAME}-${PV}" "${NODE_MODULE_NAME}"
	fi
	mkdir "${NODE_MODULE_NAME}/node_modules" || die
	for pkg in ${NODE_DEPEND}; do
		if [[ $pkg == @* ]]; then
			pkg_cat="${pkg%/*}"
			tmp="${pkg%:*}"
			pkg_name="${tmp#*/}"
			pkg_version="${pkg#*:}"
			unpack "${pkg_name}-${pkg_version}.tgz"
			if [[ -d "package" ]]; then
				mv "package" "${NODE_MODULE_NAME}/node_modules/${pkg_name}" || die
			elif [[ -d "${pkg_name}" ]]; then
				mv "${pkg_name}" "${NODE_MODULE_NAME}/node_modules/${pkg_name}" || die
			else
				die
			fi
				#statements
		else
			pkg_name="${pkg%:*}"
			if [[ $pkg_name == github ]]; then
				tmp="${pkg#*:}"
				git_user="${tmp%/*}"
				git_name="${tmp#*/}"
				if [[ $git_name == *#* ]]; then
					git_commit="${git_name#*#}"
					git_name="${git_name%#*}"
					unpack "${git_name}-${git_commit}.tar.gz"
					if [[ -d "package" ]]; then
						mv "package" "${NODE_MODULE_NAME}/node_modules/${git_name}" || die
					elif [[ -d "${git_name}" ]]; then
						mv "${git_name}" "${NODE_MODULE_NAME}/node_modules/${git_name}" || die
					elif [[ -d "${git_name}-${git_commit}" ]]; then
						mv "${git_name}-${git_commit}" "${NODE_MODULE_NAME}/node_modules/${git_name}" || die
					else
						die
					fi
				else
					unpack "${git_name}-master.tar.gz"
					if [[ -d "package" ]]; then
						mv "package" "${NODE_MODULE_NAME}/node_modules/${git_name}" || die
					elif [[ -d "${git_name}" ]]; then
						mv "${git_name}" "${NODE_MODULE_NAME}/node_modules/${git_name}" || die
					elif [[ -d "${git_name}-master" ]]; then
						mv "${git_name}-master" "${NODE_MODULE_NAME}/node_modules/${git_name}" || die
					else
						die
					fi
				fi
			else
				pkg_version="${pkg#*:}"
				unpack "${pkg_name}-${pkg_version}.tgz"
				if [[ -d "package" ]]; then
					mv "package" "${NODE_MODULE_NAME}/node_modules/${pkg_name}" || die
				elif [[ -d "${pkg_name}" ]]; then
					mv "${pkg_name}" "${NODE_MODULE_NAME}/node_modules/${pkg_name}" || die
				else
					die
				fi
			fi
		fi
	done
}

node_src_install() {
	mv "release/${NODE_RELEASE_FOLDER}" "release/${NODE_MODULE_NAME}"
	insinto "/usr/$(get_libdir)/node_modules/"
	doins -r "release/${NODE_MODULE_NAME}"
	for i in ${NODE_BIN}; do
		src="${i#*:}"
		dest="${i%:*}"
		dosym "${EROOT}/usr/$(get_libdir)/node_modules/${NODE_MODULE_NAME}/${src}" "/usr/bin/${dest}"
		fperms +x "/usr/$(get_libdir)/node_modules/${NODE_MODULE_NAME}/${src}"
	done
}
