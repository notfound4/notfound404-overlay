EAPI=7

inherit git-r3

EGIT_REPO_URI="https://github.com/ram-on/imgurbash2.git"

DESCRIPTION="A simple bash script that allows you to upload images to imgur."
HOMEPAGE="https://github.com/ram-on/imgurbash2"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	net-misc/curl
	x11-misc/xclip
"
DEPEND="${RDEPEND}"

src_install() {
	newbin ${PN} ${PN}
}