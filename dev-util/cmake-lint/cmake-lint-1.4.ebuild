# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{8..10} )

inherit distutils-r1

DESCRIPTION="cmakelint parses CMake files and reports style issues."
HOMEPAGE="https://github.com/richq/cmake-lint"
SRC_URI="https://github.com/richq/${PN}/archive/v${PVR}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	dev-util/cmake
"

DEPEND="${RDEPEND}"