# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

NODE_DEPEND="esy-solve-cudf:0.1.10
    is-ci:1.2.1
    babel-preset-env:1.7.0
    babel-preset-flow:6.23.0
    del:3.0.0
    esy-bash:0.3.20
    flow-bin:0.77.0
    fs-extra:7.0.0
    jest-cli:23.4.1
    jest-pnp-resolver:1.0.1
    jest-junit:5.0.0
    klaw:2.1.1
    minimatch:3.0.4
    outdent:0.3.0
    prettier:1.13.7
    invariant:2.2.4
    rimraf:2.6.2
    semver:5.5.0
    super-resolve:1.0.0
    tar:4.4.4
    tar-fs:1.16.0
    tmp:0.0.33"
NODE_BIN="esy:_build/default/bin/esy.exe"
#NODEJS_MIN_VERSION="0.10.0"

inherit node

DESCRIPTION="package.json workflow for native development with Reason/OCaml."
HOMEPAGE="https://github.com/esy/esy"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

src_unpack() {
	node_src_unpack
	rm -r ${S}/platform-darwin || die
	rm -r ${S}/platform-win32 || die
	rm -r ${S}/node_modules/esy-bash/.cygwin || die
}

src_install() {
	node ./postinstall.js
	node_src_install
}