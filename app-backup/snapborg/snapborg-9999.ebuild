# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

PYTHON_COMPAT=( python3_{9..12} )

inherit distutils-r1 git-r3

DESCRIPTION="Automated backups of snapper snapshots to borg repositories."
HOMEPAGE="https://github.com/enzingerm/snapborg"
EGIT_REPO_URI="https://github.com/enzingerm/snapborg"

LICENSE="GPL3+"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	dev-python/pyyaml[${PYTHON_USEDEP}]
"

DEPEND="${RDEPEND}"
