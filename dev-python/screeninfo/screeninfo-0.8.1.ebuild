# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..12} )

inherit distutils-r1

DESCRIPTION="Fetch location and size of physical screens."
HOMEPAGE="https://github.com/rr-/screeninfo"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

distutils_enable_tests pytest

DEPEND="
	test? (
		dev-python/pytest[${PYTHON_USEDEP}]
	)
"
