# Copyright 1999-2022 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9..12} )

inherit distutils-r1

DESCRIPTION="Minimal R API for Python"
HOMEPAGE="https://github.com/randy3k/rchitect"
SRC_URI="https://github.com/randy3k/${PN}/archive/v${PVR}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	>=dev-python/cffi-1.10.0[${PYTHON_USEDEP}]
	>=dev-python/six-1.9.0[${PYTHON_USEDEP}]
"

DEPEND="${RDEPEND}
	dev-python/pytest[${PYTHON_USEDEP}]
"

distutils_enable_tests pytest