# Copyright 1999-2022 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..11} )

inherit distutils-r1

DESCRIPTION="Minimal R API for Python"
HOMEPAGE="https://github.com/randy3k/rchitect"
SRC_URI="https://github.com/randy3k/${PN}/archive/v${PVR}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	>=dev-python/cffi-1.10.0[${PYTHON_USEDEP}]
	>=dev-python/six-1.9.0[${PYTHON_USEDEP}]
"

DEPEND="${RDEPEND}
	dev-python/pytest[${PYTHON_USEDEP}]
"

RESTRICT="test"

src_prepare()
{
	sed -i -e "s/pytest-runner/pytest/" setup.py
	elog "prepare"
	distutils-r1_src_prepare
}

src_configure()
{
	elog "configure"
	distutils-r1_src_configure
}

src_compile()
{
	elog "compile"
	distutils-r1_src_compile
}

src_install()
{
	elog "install"
	distutils-r1_src_install
}
