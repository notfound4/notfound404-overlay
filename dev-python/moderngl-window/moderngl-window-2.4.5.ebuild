# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9..12} )

inherit distutils-r1

DESCRIPTION="A cross platform utility library for ModernGL making window creation and resource loading simple."
HOMEPAGE="https://github.com/moderngl/moderngl-window"
SRC_URI="https://github.com/moderngl/${PN}/archive/refs/tags/${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	<dev-python/moderngl-6.0.0[${PYTHON_USEDEP}]
	>=dev-python/numpy-1.16[${PYTHON_USEDEP}]
	!>=dev-python/numpy-2.0
	>=dev-python/pyglet-2.0.0[${PYTHON_USEDEP}]
	>=dev-python/pyrr-0.10.3[${PYTHON_USEDEP}]
	!>=dev-python/pyrr-1.0.0
	>=dev-python/pillow-9.0.0[${PYTHON_USEDEP}]
"