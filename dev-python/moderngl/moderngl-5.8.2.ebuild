# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..12} )

inherit distutils-r1

DESCRIPTION="Modern OpenGL bindings for python."
HOMEPAGE="https://github.com/moderngl/moderngl"
SRC_URI="https://github.com/moderngl/${PN}/archive/refs/tags/${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	>=dev-python/glcontext-2.3.6[${PYTHON_USEDEP}]
	!>=dev-python/glcontext-3.0
"
