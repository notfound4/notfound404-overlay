# Copyright 1999-2021 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

PYTHON_COMPAT=( python3_{9..12} )

inherit distutils-r1

DESCRIPTION="Pydub lets you do stuff to audio in a way that isn't stupid."
HOMEPAGE="https://github.com/jiaaro/pydub/"
SRC_URI="https://github.com/jiaaro/${PN}/archive/refs/tags/v${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND=""

DEPEND="${RDEPEND}
	dev-python/pytest[${PYTHON_USEDEP}]
"