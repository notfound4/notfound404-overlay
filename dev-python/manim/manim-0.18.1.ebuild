# Copyright 1999-2021 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

DISTUTILS_USE_PEP517=poetry
PYTHON_COMPAT=( python3_{9..12} )

inherit distutils-r1


DESCRIPTION="An animation engine for explanatory math videos."
HOMEPAGE="https://github.com/ManimCommunity/manim"
SRC_URI="https://github.com/ManimCommunity/manim/archive/refs/tags/v${PV}.tar.gz"
#SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	>=dev-python/click-8.0.0[${PYTHON_USEDEP}]
	dev-python/click-default-group[${PYTHON_USEDEP}]
	>=dev-python/cloup-2.0.0[${PYTHON_USEDEP}]
	dev-python/decorator[${PYTHON_USEDEP}]
	=dev-python/isosurfaces-0.1.0[${PYTHON_USEDEP}]
	=dev-python/manimpango-0.5.0[${PYTHON_USEDEP}]
	>=dev-python/mapbox-earcut-1.0.0[${PYTHON_USEDEP}]
	>=dev-python/moderngl-5.6.3[${PYTHON_USEDEP}]
	!>=dev-python/moderngl-6.0.0
	>=dev-python/moderngl-window-2.3.0[${PYTHON_USEDEP}]
	!>=dev-python/moderngl-window-3.0.0
	>=dev-python/networkx-2.6[${PYTHON_USEDEP}]
	dev-python/numpy[${PYTHON_USEDEP}]
	dev-python/pillow[${PYTHON_USEDEP}]
	>=dev-python/pycairo-1.21.0[${PYTHON_USEDEP}]
	!>=dev-python/pycairo-2.0.0
	dev-python/pydub[${PYTHON_USEDEP}]
	dev-python/pygments[${PYTHON_USEDEP}]
	>=dev-python/rich-12.0.0[${PYTHON_USEDEP}]
	dev-python/scipy[${PYTHON_USEDEP}]
	>=dev-python/screeninfo-0.8[${PYTHON_USEDEP}]
	!>=dev-python/screeninfo-0.9
	>=dev-python/skia-pathops-0.7.0[${PYTHON_USEDEP}]
	>=dev-python/srt-3.5.0[${PYTHON_USEDEP}]
	>=dev-python/svgelements-1.8.0[${PYTHON_USEDEP}]
	>=dev-python/typing-extensions-4.7.1[${PYTHON_USEDEP}]
	dev-python/tqdm[${PYTHON_USEDEP}]
	>=dev-python/watchdog-2.1.6[${PYTHON_USEDEP}]
"

DEPEND="${RDEPEND}
	dev-python/pytest[${PYTHON_USEDEP}]
"

# S="${WORKDIR}"/${MY_P}
