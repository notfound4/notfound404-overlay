# Copyright 1999-2022 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..11} )

inherit distutils-r1

MY_PVR="0.4.0.post2"
MY_P="ManimPango-${MY_PVR}"

DESCRIPTION="ManimPango is a C binding for Pango using Cython, which is internally used in Manim to render (non-LaTeX) text."
HOMEPAGE="https://github.com/ManimCommunity/ManimPango"
SRC_URI="https://github.com/ManimCommunity/${PN}/archive/refs/tags/v${MY_PVR}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	x11-libs/cairo
	x11-libs/pango
	dev-libs/glib
	dev-libs/gobject-introspection
	dev-libs/icu:=
	media-libs/harfbuzz
"

DEPEND="${RDEPEND}
	dev-python/pytest[${PYTHON_USEDEP}]
"

S="${WORKDIR}"/${MY_P}