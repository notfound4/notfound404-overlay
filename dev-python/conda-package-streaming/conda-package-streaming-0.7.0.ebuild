# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=flit
PYTHON_COMPAT=( python3_{9..12} )
inherit distutils-r1

DESCRIPTION="Download conda metadata from packages without transferring entire file"
HOMEPAGE="https://github.com/conda/conda-package-streaming"
SRC_URI="https://github.com/conda/conda-package-streaming/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="amd64 ~x86"

DEPEND="
	>=dev-python/zstandard-0.15[${PYTHON_USEDEP}]
	dev-python/requests[${PYTHON_USEDEP}]"

RDEPEND="${DEPEND}"
BDEPEND=""

distutils_enable_tests pytest

python_prepare_all() {
	# sed -i 's/archive_and_deps/archive/' setup.py || die
	distutils-r1_python_prepare_all
}
