# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..12} )

inherit distutils-r1

DESCRIPTION="A readline library based on prompt_toolkit which supports multiple modes"
HOMEPAGE="https://github.com/randy3k/lineedit"
SRC_URI="https://github.com/randy3k/${PN}/archive/v${PVR}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	dev-python/pygments[${PYTHON_USEDEP}]
	dev-python/wcwidth[${PYTHON_USEDEP}]
	>=dev-python/six-1.9.0[${PYTHON_USEDEP}]
"

DEPEND="${RDEPEND}"

