EAPI=7

inherit git-r3

EGIT_REPO_URI="https://github.com/wknapik/vpnfailsafe.git"

DESCRIPTION="vpnfailsafe prevents a VPN user's ISP-assigned IP address from being exposed on the internet, both while the VPN connection is active and when it goes down."
HOMEPAGE="https://github.com/wknapik/vpnfailsafe"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	net-dns/openresolv
	net-vpn/openvpn
	sys-apps/iproute2
	net-firewall/iptables
	sys-process/procps
"
DEPEND="${RDEPEND}"

src_install() {
	exeinto /etc/openvpn
	newexe vpnfailsafe.sh vpnfailsafe
	dobin extras/vpnfailsafe_reset.sh
}