EAPI=7

inherit git-r3

EGIT_REPO_URI="https://github.com/masterkorp/openvpn-update-resolv-conf.git"

DESCRIPTION="This is a script to update your /etc/resolv.conf with DNS settings that come from the received push dhcp-options."
HOMEPAGE="https://github.com/masterkorp/openvpn-update-resolv-conf"

LICENSE="GNU GPL"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	net-dns/openresolv
	net-vpn/openvpn
"
DEPEND="${RDEPEND}"

src_install() {
	exeinto /etc/openvpn
	newexe update-resolv-conf.sh update-resolv-conf
}