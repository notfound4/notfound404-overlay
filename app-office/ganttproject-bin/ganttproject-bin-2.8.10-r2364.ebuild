# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit java-pkg-2 desktop

MY_P=${PN/-bin}-${PV}
MY_PF=${PN/-bin}-${PVR}
DESCRIPTION="A tool for creating a project schedule by means of Gantt chart and resource load chart"
HOMEPAGE="https://github.com/bardsoftware/ganttproject"
SRC_URI="https://github.com/bardsoftware/ganttproject/releases/download/${MY_P}/${MY_PF}.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-arch/unzip
	>=virtual/jdk-1.7"
RDEPEND=">=virtual/jre-1.7"

S="${WORKDIR}/${MY_PF}"

src_install() {
	insinto /usr/share/${PN}
	doins -r eclipsito.jar plugins-${PV}/ || die

	newbin "${FILESDIR}/${PV}-${PN}" ${PN} || die

	insinto /usr/share/${PN}/examples
	doins *.gan || die

	doicon "${S}/plugins-${PV}/ganttproject/data/resources/icons/ganttproject.png"
	make_desktop_entry ${PN} "GanttProject" ${PN/-bin} "Java;Office;ProjectManagement"
}
