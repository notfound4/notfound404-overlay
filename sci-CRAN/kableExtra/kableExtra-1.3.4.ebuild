# Copyright 1999-2021 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit R-packages

DESCRIPTION='Construct Complex Table with knitr::kable() + pipe.'
KEYWORDS="~amd64"
SRC_URI="http://cran.r-project.org/src/contrib/kableExtra_1.3.4.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_testthat r_suggests_magick r_suggests_formattable r_suggests_dplyr"
R_SUGGESTS="
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_magick? ( sci-CRAN/magick )
	r_suggests_formattable? ( sci-CRAN/formattable )
	r_suggests_dplyr? ( sci-CRAN/dplyr )
"
DEPEND=">=sci-CRAN/knitr-1.13
	sci-CRAN/magrittr
	>=sci-CRAN/stringr-1.0
	sci-CRAN/xml2
	>=dev-lang/R-3.1.0
	sci-CRAN/rvest
	>=sci-CRAN/rmarkdown-1.6.0
	sci-CRAN/readr
	sci-CRAN/scales
	sci-CRAN/viridisLite
	sci-CRAN/svglite
"
RDEPEND="${DEPEND-}
	${R_SUGGESTS-}
"

_UNRESOLVED_PACKAGES=(
	'sci-CRAN/webshot'
)
