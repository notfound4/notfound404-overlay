Notfound404 Overlay
====================

My personal overlay of ebuild for Gentoo or Gentoo based distributions that support portage.


Setup
-----

layman -o https://gitlab.com/notfound4/notfound404-overlay/raw/master/notfound404.xml -f -a notfound404


Where can I send feedbacks?
----------------------------

Use this repository issues system
